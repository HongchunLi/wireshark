#ifndef SYNC_WORKER_H
#define SYNC_WORKER_H

#include "glib.h"
#include <QThread>
#include <QTextStream>
#include <QLocalSocket>


class SyncWorker : public QThread
{
   Q_OBJECT

private:
    QLocalSocket socketIn;
    QLocalSocket socketOut;

public:
    void run();
    SyncWorker(QString parms);
    ~SyncWorker();
    guint64 lastTs;
    int lastIndex;


signals:
    void selectFrame(long long timeStamp, int iindex);

public slots:
    void selectionChanged(long long timeStamp, guint32 iindex);



    //deleteLater();
};

#endif
