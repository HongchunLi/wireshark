#include <QTextStream>
#include <QDataStream>
#include "sync_worker.h"

SyncWorker::SyncWorker(QString parms)
{
    QStringList qsList = parms.split(':');
    if (qsList.size() == 2)
    {
        socketIn.connectToServer(qsList.at(0), QIODevice::ReadOnly);
        socketIn.waitForConnected(5000);
        socketOut.connectToServer(qsList.at(1), QIODevice::WriteOnly);
        socketOut.waitForConnected(5000);
    }
    lastTs = 0;
    lastIndex = 0;
}

SyncWorker::~SyncWorker()
{
    socketIn.close();
    socketOut.close();
}

void SyncWorker::run()
{
    QTextStream reader(&socketIn);
    while (socketIn.state() == QLocalSocket::ConnectedState) {
        int len = socketIn.bytesAvailable();
        if (len <= 0)
        {
            QThread::msleep(100);
            continue;
        }
        QString line = reader.readLine();
        if (line.size() > 0)
        {
            lastTs = 0;
            lastIndex = 0;
            QStringList strList = line.split(':');
            if (strList.size() == 2 && strList.at(0) == "SelectTime")
            {
                bool ok = false;
                lastTs = strList.at(1).toLongLong(&ok);
                if (lastTs > 0)
                {
                    emit selectFrame(lastTs, 0);
                }
            }
            else if (strList.size() == 2 && strList.at(0) == "SelectIndex")
            {
                bool ok = false;
                lastIndex = strList.at(1).toInt(&ok);
                if (lastIndex > 0)
                {
                    emit selectFrame(0, lastIndex);
                }
            }
            QThread::msleep(100);
        }
    }
}

void SyncWorker::selectionChanged(long long timeStamp, guint32 iindex)
{
    if (lastIndex > 0) {
        if (lastIndex == iindex) { return; }
    }
    else if (lastTs > 0 && lastTs == timeStamp) { return; }

    lastTs = timeStamp;
    lastIndex = iindex;
    if (socketOut.state() == QLocalSocket::ConnectedState)
    {
        GString* buffer = g_string_new("");
        g_string_printf(buffer, "SC:%I64u;SI:%u\n", (guint64)timeStamp, iindex);
        socketOut.write(buffer->str, buffer->len);
        socketOut.flush();

        g_string_free(buffer, TRUE);
    }
}

